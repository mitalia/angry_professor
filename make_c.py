#!/usr/bin/env python3
import sys
print("asm(")
for l in sys.stdin:
    l=l.rstrip().replace('\\','\\\\').replace('"', '\\"')
    print('    "%s\\n"' % l)
print(");")
