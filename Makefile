all: angry

clean:
	rm -f angry.c angry

angry.c: angry.s make_c.py
	./make_c.py < angry.s > angry.c

angry: angry.c
	gcc -g -O3 angry.c -oangry
