x86\_64 assembly solution to https://www.hackerrank.com/contests/coding-gym-mi0219/challenges/angry-professor/

As HackerRank doesn't contemplate assembly as a language for challenges, an intermediate "conversion to C" is performed by the Makefile. After executing `make` an `angry.c` file is produced. Its content can be copy-pasted in HackerRank _as C++14_ (as it uses `g++`/GNU as on Linux, unlike C, which is probably using LLVM, which has some difference in the assembler).
