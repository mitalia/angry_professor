.intel_syntax noprefix

.data
buf_start:  .int 0          # read pointer in the buffer
buf_end:    .int 0          # end of buffer
cur_char:   .int -1         # current character being parsed
in_buffer:  .fill 16384     # read buffer
YES:        .ascii "YES\n"
NO:         .ascii "NO\n"

.text
.globl main

# thin wrapper around write syscall
# ssize_t bare_write(int fd, const void *buf, size_t size);
bare_write:
    xor eax,eax
    inc eax
    syscall
    ret

# refills in_buffer, reading from STDIN
# void refill_in_buffer() {
#     ssize_t ret = read(0, in_buffer, 16384);
#     if(ret <= 0) buf_end = 0;
#     else {
#         buf_end = ret;
#         buf_start = 0;
#     }
# }
refill_in_buffer:
    xor eax,eax
    xor edi,edi
    lea rsi,[in_buffer + rip]
    mov edx,16384
    syscall
    test rax,rax
    jg .ok
    xor eax,eax
.ok:
    mov [buf_end + rip],rax
    xor rax,rax
    mov [buf_start + rip],eax
    ret

# reads the next character, putting it into cur_char;
# puts -1 if an error (EOF or whatsnot) occurred when refilling
# void get_char() {
#     if(buf_start == buf_end) refill_in_buffer();
#     if(buf_start == buf_end) cur_char = -1;
#     else {
#         cur_char = in_buffer[buf_start];
#         buf_start++;
#     }
# }
get_char:
    mov eax,[buf_start + rip]
    cmp [buf_end + rip],eax
    jne .filled_1
    call refill_in_buffer
.filled_1:
    mov eax,[buf_start + rip]
    cmp [buf_end + rip],eax
    jne .filled_2
    mov dword ptr[cur_char + rip], -1
    ret
.filled_2:
    lea rdx,[in_buffer + rip]
    movsx eax, byte ptr[rdx + rax]
    mov [cur_char + rip],eax
    inc dword ptr[buf_start + rip]
    ret

# skips all the whitespace (space, \n, \t)
# void skip_whitespace() {
#     while(cur_char == ' ' || cur_char == '\n' || cur_char == '\t') get_char();
# }
skip_whitespace:
    mov eax,dword ptr[cur_char + rip]
    cmp eax,' '
    je .sw_lop
    cmp eax,'\n'
    je .sw_lop
    cmp eax,'\t'
    je .sw_lop
    ret
.sw_lop:
    call get_char
    jmp skip_whitespace

# reads a signed integer, skipping whitespace
# int get_int() {
#     skip_whitespace();
#     int ret = 0;  // <- rbx
#     int sgn = 0;  // <- r12d
#     if(cur_char == '-') {
#         --sgn;
#         get_char();
#     }
#     for(;;) {
#         int eax = cur_char - '0';
#         if(eax < 0 || eax > 9) break;
#         ret *= 10;
#         ret += eax;
#         get_char();
#     }
#     if(sgn) ret = -ret;
#     return ret;
# }
get_int:
    call skip_whitespace
    push rbx
    push r12
    xor ebx,ebx
    xor r12d,r12d
    cmp dword ptr[cur_char + rip],'-'
    jne .num_loop
    dec r12d
    call get_char
.num_loop:
    mov eax,[cur_char + rip]
    sub eax,'0'
    jl .done_numbers
    cmp eax,9
    jg .done_numbers
    imul ebx,10
    add ebx,eax
    call get_char
    jmp .num_loop
.done_numbers:
    test r12d,r12d
    jz .positive
    neg ebx
.positive:
    mov eax,ebx
    pop r12
    pop rbx
    ret

# performs a single iteration of the "angry professor" challenge
# void angry() {
#     int n_students = get_int();   // ebx
#     int threshold = get_int();    // r12d
#     int good_students = 0;        // r13d
#     do {
#         int time = get_int();
#         if(time <= 0) ++good_students;
#     } while(--n_students);
#     if(good_students >= threshold) write(0, "NO\n", 3);
#     else                           write(0, "YES\n", 4);
# }
angry:
    push rbx        # number of students
    push r12        # threshold
    push r13        # good students count
    call get_int
    mov ebx,eax
    call get_int
    mov r12d,eax
    xor r13d,r13d
    # begin student read loop
.read_loop:
    call get_int
    test eax,eax
    jg .late
    # the student was not late
    inc r13d
.late:
    dec ebx
    jg .read_loop
    # end loop
    # check threshold
    cmp r13d,r12d
    jge .print_no
    lea rsi,[YES + rip]
    mov edx,4
    jmp .do_print
.print_no:
    lea rsi,[NO + rip]
    mov edx,3
.do_print:
    # keep the call common to the print yes/print no branches
    xor edi,edi
    inc edi
    call bare_write
    pop r13
    pop r12
    pop rbx
    ret

# int main() {
#     get_char(); // start parsing
#     for(int i = get_int(); i; --i) {
#         angry();
#     }
#     return 0;
# }
main:
    push rbx
    call get_char # start parsing
    call get_int
    mov ebx,eax
    jmp .lop_coda
.lop:
    call angry
    dec ebx
.lop_coda:
    test ebx,ebx
    jnz .lop
    xor eax,eax
    pop rbx
    ret
